#ifndef _CDC_H_
#define _CDC_H_

#include "stm32h7xx_hal.h"
#include "usbd_cdc_if.h"


#define CDC_BUFFER_SIZE (256)
#define CDC_CMD_LENGTH   16


typedef struct
{
	uint16_t rxLength;
	uint8_t  rxEnd;
	uint32_t rxRemindBufferSize;
	uint8_t  rxBuffer[CDC_BUFFER_SIZE];
	uint8_t  rxNew2Uart;
}cdc_t;

extern cdc_t cdc;

void cdc_send(uint8_t* Buf, uint32_t Len);
void cdc_printf(const char *format, ...);
void cdc_cli_uart2usb(void);
void cdc_cli_task(void);
void cdc_clear_buffer(void);
#endif

