/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ENABLE_SD_DMA_CACHE_MAINTENANCE 1
#define LED_RED_Pin GPIO_PIN_5
#define LED_RED_GPIO_Port GPIOE
#define LED_GREEN_Pin GPIO_PIN_6
#define LED_GREEN_GPIO_Port GPIOE
#define MEM_PSRAMCS_Pin GPIO_PIN_13
#define MEM_PSRAMCS_GPIO_Port GPIOC
#define MEM_FLASHCS_Pin GPIO_PIN_0
#define MEM_FLASHCS_GPIO_Port GPIOC
#define MEM_MISO_Pin GPIO_PIN_2
#define MEM_MISO_GPIO_Port GPIOC
#define MEM_MOSI_Pin GPIO_PIN_3
#define MEM_MOSI_GPIO_Port GPIOC
#define MEM_SCK_Pin GPIO_PIN_10
#define MEM_SCK_GPIO_Port GPIOB
#define SDMMC_CDN_Pin GPIO_PIN_8
#define SDMMC_CDN_GPIO_Port GPIOD
#define USB_N_Pin GPIO_PIN_11
#define USB_N_GPIO_Port GPIOA
#define USB_P_Pin GPIO_PIN_12
#define USB_P_GPIO_Port GPIOA
#define D40_TMS_Pin GPIO_PIN_13
#define D40_TMS_GPIO_Port GPIOA
#define D41_TCK_Pin GPIO_PIN_14
#define D41_TCK_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
